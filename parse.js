const fs = require('fs');
const readline = require('readline');

const fileStream = fs.createReadStream('sii/PUB_EMPRESAS_PJ_2020_A_2024.tsv', 'utf8');

const fileIndex = fs.readFileSync('index.txt', 'utf8');

console.log(`ultimo indice es: ${fileIndex}`);

const rl = readline.createInterface({
    input: fileStream,
    crlfDelay: Infinity
});

const YEAR = 0;
const RUT = 1;
const DV = 2;
const RAZON_SOCIAL = 3;
const TRAMO = 4;
const NUMERO_TRABAJADORES = 5;
const FECHA_INICIO = 6;
const FECHA_TERMINO = 7;
const FECHA_INSCRIPCION = 8;
const TIPO_TERMINO_GIRO = 9;
const TIPO_CONTRIBUYENTE = 10;
const SUBTIPO_CONTRIBUYENTE = 11;
const TRAMO_CAPITAL_POSITIVO = 12;
const TRAMO_CAPITAL_NEGATIVO = 13;
const RUBRO_ECONOMICO = 14;
const SUBRUBRO_ECONOMICO = 15;
const ACTIVIDAD_ECONOMICA = 16;
const REGION = 17;
const PROVINCIA = 18;
const COMUNA = 19;

var currentLine = 0;
rl.on('line', (line) => {
    //console.log(`Line: ${line}`);
    const tabs = line.split('\t')
    currentLine++;

    if (currentLine > fileIndex){
        //crea la carpeta del rut si no existe
        if (!fs.existsSync(`result/${tabs[RUT]}`)) fs.mkdirSync(`result/${tabs[RUT]}`);

        const body = {
            rut: tabs[RUT],
            dv: tabs[DV],
            year: tabs[YEAR],
            razonSocial: tabs[RAZON_SOCIAL],
            tramo: tabs[TRAMO],
            numeroTrabajadores: tabs[NUMERO_TRABAJADORES],
            fechaInicio: tabs[FECHA_INICIO],
            fechaTermino: tabs[FECHA_TERMINO],
            fechaInscripcion: tabs[FECHA_INSCRIPCION],
            tipoTerminoGiro: tabs[TIPO_TERMINO_GIRO],
            tipoContribuyente: tabs[TIPO_CONTRIBUYENTE],
            subTipoContribuyente: tabs[SUBTIPO_CONTRIBUYENTE],
            tramoCapitalPositivo: tabs[TRAMO_CAPITAL_POSITIVO],
            tramoCapitalNegativo: tabs[TRAMO_CAPITAL_NEGATIVO],
            ruboEconomico: tabs[RUBRO_ECONOMICO],
            subRubroEconomico: tabs[SUBRUBRO_ECONOMICO],
            actividadEconomica: tabs[ACTIVIDAD_ECONOMICA],
            region: tabs[REGION],
            provincia: tabs[PROVINCIA],
            comuna: tabs[COMUNA]
        }

        fs.writeFileSync(`result/${tabs[RUT]}/nomina_empresa.json`, JSON.stringify(body))
        console.log(`archivo creado para el rut ${tabs[RUT]}`)
        
        fs.writeFileSync(`index.txt`, JSON.stringify(currentLine))
        console.log(`ultima linea ${currentLine}`)
        
    }

});

rl.on('close', () => {
    console.log('Finished reading the file.');
});