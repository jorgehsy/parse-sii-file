import { S3Client, GetObjectCommand, PutObjectCommand } from'@aws-sdk/client-s3';
const client = new S3Client({});

const YEAR = 0;
const RUT = 1;
const DV = 2;
const RAZON_SOCIAL = 3;
const TRAMO = 4;
const NUMERO_TRABAJADORES = 5;
const FECHA_INICIO = 6;
const FECHA_TERMINO = 7;
const FECHA_INSCRIPCION = 8;
const TIPO_TERMINO_GIRO = 9;
const TIPO_CONTRIBUYENTE = 10;
const SUBTIPO_CONTRIBUYENTE = 11;
const TRAMO_CAPITAL_POSITIVO = 12;
const TRAMO_CAPITAL_NEGATIVO = 13;
const RUBRO_ECONOMICO = 14;
const SUBRUBRO_ECONOMICO = 15;
const ACTIVIDAD_ECONOMICA = 16;
const REGION = 17;
const PROVINCIA = 18;
const COMUNA = 19;

const bucketRaw = 'guou-raw-layer';
const bucketCurated = 'guou-curated-layer';
const path = 'sii/nomina_empresas_sii';

const parseLine = (line) =>{
  return new Promise(async (resolve, reject) => {
    const tabs = line.split('\t')
    console.log("LINEA ", tabs)
    const body = {
      rut: tabs[RUT],
      dv: tabs[DV],
      year: tabs[YEAR],
      razonSocial: tabs[RAZON_SOCIAL],
      tramo: tabs[TRAMO],
      numeroTrabajadores: tabs[NUMERO_TRABAJADORES],
      fechaInicio: tabs[FECHA_INICIO],
      fechaTermino: tabs[FECHA_TERMINO],
      fechaInscripcion: tabs[FECHA_INSCRIPCION],
      tipoTerminoGiro: tabs[TIPO_TERMINO_GIRO],
      tipoContribuyente: tabs[TIPO_CONTRIBUYENTE],
      subTipoContribuyente: tabs[SUBTIPO_CONTRIBUYENTE],
      tramoCapitalPositivo: tabs[TRAMO_CAPITAL_POSITIVO],
      tramoCapitalNegativo: tabs[TRAMO_CAPITAL_NEGATIVO],
      ruboEconomico: tabs[RUBRO_ECONOMICO],
      subRubroEconomico: tabs[SUBRUBRO_ECONOMICO],
      actividadEconomica: tabs[ACTIVIDAD_ECONOMICA],
      region: tabs[REGION],
      provincia: tabs[PROVINCIA],
      comuna: tabs[COMUNA]
    }

    const key = `${path}/${tabs[RUT]}/nomina.json`
    console.log(`SAVING... ${key}`)

    try{
      await client.send(new PutObjectCommand({
        Bucket: bucketCurated,
        Key: key,
        Body: JSON.stringify(body)
      }))
      resolve("File created ", key)
    }catch (e){
      reject("ERROR on ", key)
      console.log("ERROR: ", e)
    }
  })
};

const streamToString = (stream) => new Promise((resolve, reject) => {
  const chunks = [];
  stream.on('data', (chunk) => chunks.push(chunk));
  stream.on('error', reject);
  stream.on('end', () => resolve(Buffer.concat(chunks).toString('utf8')));
});

const readFile = async (bucket, key) => {
  const params = {
    Bucket: bucket,
    Key: key,
  };

  const command = new GetObjectCommand(params);
  const response = await client.send(command);

  const { Body } = response;

  return streamToString(Body);
};

export const handler = async(event) => {
  const sourceKey = `${path}/test.txt`

  const fileRaw =  await readFile(bucketRaw, sourceKey);
  const lines = fileRaw.split('\n');

  for (let line in lines){
    const result = await parseLine(lines[line]);
    console.log("RESULT: ", result)
  }
};
