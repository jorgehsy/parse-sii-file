import { S3Client, PutObjectCommand } from '@aws-sdk/client-s3';
import fs from 'fs';
import readline from 'readline';

const client = new S3Client({
  credentials: {
    accessKeyId: 'ASIAQYF3G6327WQDU6WH',
    secretAccessKey: 'hk2RbUsfebYuCE1fN2z+Lt9FJMdFNTia70hfJIRV'
  },
  region: 'us-west-2'
});

const YEAR = 0;
const RUT = 1;
const DV = 2;
const RAZON_SOCIAL = 3;
const TRAMO = 4;
const NUMERO_TRABAJADORES = 5;
const FECHA_INICIO = 6;
const FECHA_TERMINO = 7;
const FECHA_INSCRIPCION = 8;
const TIPO_TERMINO_GIRO = 9;
const TIPO_CONTRIBUYENTE = 10;
const SUBTIPO_CONTRIBUYENTE = 11;
const TRAMO_CAPITAL_POSITIVO = 12;
const TRAMO_CAPITAL_NEGATIVO = 13;
const RUBRO_ECONOMICO = 14;
const SUBRUBRO_ECONOMICO = 15;
const ACTIVIDAD_ECONOMICA = 16;
const REGION = 17;
const PROVINCIA = 18;
const COMUNA = 19;

const bucketCurated = 'guou-curated-layer';
const path = 'sii/nomina_empresas_sii';
const rawKey = 'test.txt';

const parseFile = async () => {

  return new Promise(function (resolve, reject) {
    const fileStream = fs.createReadStream(`sii/${rawKey}`, 'utf8');

    const rl = readline.createInterface({
      input: fileStream,
      crlfDelay: Infinity
    });

    let currentLine = 0;

    rl.on('line', async (line) => {
      const tabs = line.split('\t')


      if (currentLine > 0){
        const body = {
          rut: tabs[RUT],
          dv: tabs[DV],
          year: tabs[YEAR],
          razonSocial: tabs[RAZON_SOCIAL],
          tramo: tabs[TRAMO],
          numeroTrabajadores: tabs[NUMERO_TRABAJADORES],
          fechaInicio: tabs[FECHA_INICIO],
          fechaTermino: tabs[FECHA_TERMINO],
          fechaInscripcion: tabs[FECHA_INSCRIPCION],
          tipoTerminoGiro: tabs[TIPO_TERMINO_GIRO],
          tipoContribuyente: tabs[TIPO_CONTRIBUYENTE],
          subTipoContribuyente: tabs[SUBTIPO_CONTRIBUYENTE],
          tramoCapitalPositivo: tabs[TRAMO_CAPITAL_POSITIVO],
          tramoCapitalNegativo: tabs[TRAMO_CAPITAL_NEGATIVO],
          ruboEconomico: tabs[RUBRO_ECONOMICO],
          subRubroEconomico: tabs[SUBRUBRO_ECONOMICO],
          actividadEconomica: tabs[ACTIVIDAD_ECONOMICA],
          region: tabs[REGION],
          provincia: tabs[PROVINCIA],
          comuna: tabs[COMUNA]
        }

        const fileKey = `${path}/${tabs[RUT]}/nomina.json`;
        rl.pause();
        try{
          await client.send(new PutObjectCommand({
            Bucket: bucketCurated,
            Key: fileKey,
            Body: JSON.stringify(body)
          }))

          console.log("File created: ", fileKey)
        } catch(e){
          console.log(`ERROR at ${fileKey}`)
          console.log(e)
          reject(`ERROR at ${fileKey}`)
        } finally {
          rl.resume()
        }
      }

      currentLine++;
    });

    rl.on('close', () => {
      resolve("finished")
    })
  })
}

await parseFile();

